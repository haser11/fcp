package FCP.Players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import FCP.Config.ConfigManager;
import FCP.main.BasicInfo;

public class MedicListener implements Listener
{
	static Plugin plugin;
	static List<String> list = new ArrayList<String>();
	ConfigManager cm = new ConfigManager(plugin);
	

	public MedicListener(Plugin p)
	{
		MedicListener.plugin = p;
	}
	
	//Called when a player right clicks another player and heals the right clicked player if conditions are met
	@EventHandler
	public void onClickPlayer(PlayerInteractEntityEvent event)
	{
		if(event.getRightClicked() instanceof Player && !list.contains(event.getPlayer().getName()) && 
				cm.getString(event.getPlayer(), BasicInfo.PLAYERFACTIONPATH).equalsIgnoreCase("Physicians"))
		{
			double oldHealth = ((Player) event.getRightClicked()).getHealth();
			if(oldHealth < 20.0)
			{
				double newHealth = oldHealth + 12.0;
				((Player) event.getRightClicked()).setHealth(newHealth);
				event.getPlayer().sendMessage(ChatColor.BLUE + "[Faction] " + ChatColor.GREEN + "You have successfully healed " + 
				((Player) event.getRightClicked()).getName());
				((Player) event.getRightClicked()).sendMessage(ChatColor.BLUE + "[Faction] " + ChatColor.GREEN + "You have been healed by " + event.getPlayer().getName());
				list.add(event.getPlayer().getName());
			}
		}
		else
		{
			event.getPlayer().sendMessage(ChatColor.BLUE + "[Faction] " + ChatColor.RED + "You can't do that!");
		}
	}
	
	//Called when a player left clicks the air or a block and heals them if conditions are met
	@EventHandler
	public void onClickAir(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK))
		{
			if(!list.contains(event.getPlayer().getName()) && cm.getString(event.getPlayer(), BasicInfo.PLAYERFACTIONPATH).equalsIgnoreCase("Physicians"))
			{
				double oldHealth = event.getPlayer().getHealth();
				
				if(oldHealth < 20.0)
				{
					double newHealth = oldHealth + 12.0;
					event.getPlayer().setHealth(newHealth);
					list.add(event.getPlayer().getName());
				}
			}
		}
	}
	
	//Clears hashmap
	public static void clearList()
	{
		list.clear();
	}
	
	public static void startTimer()
	{
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new BukkitRunnable()
		{

			@Override
			public void run() 
			{
				clearList();
			}
			
		}, 0L, 6000L);
	}
}
