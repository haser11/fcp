package FCP.Players;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import FCP.Config.ConfigManager;
import FCP.Factions.Faction;
import FCP.main.BasicInfo;

public class CommandManager implements CommandExecutor
{
	Plugin plugin;
	
	public CommandManager(Plugin p)
	{
		this.plugin = p;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) 
	{
		if(sender instanceof Player)
		{
			Player p = (Player) sender;
		if(command.getName().equalsIgnoreCase("fcp"))
		{
			if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase("check"))
				{
					Player target = Bukkit.getServer().getPlayer(args[1]);
					ConfigManager cm = new ConfigManager(plugin);
					sender.sendMessage(BasicInfo.FACTIONTAG + target + " is a part of the faction " + cm.getString(target, BasicInfo.PLAYERFACTIONPATH));				
				}
			}
			if(args.length == 4)
			{
				if(args[0].equalsIgnoreCase("setfaction"))
				{
					Player target = Bukkit.getPlayerExact(args[1]);
					new Faction(args[2], target, args[3]);
					Bukkit.getServer().broadcastMessage(ChatColor.BLUE + "[Faction] " + ChatColor.GREEN + "User " + p.getName() + " has joined the Faction " + args[2]);
					return true;
				}
			}
		}
		}
		return false;
	}

}
