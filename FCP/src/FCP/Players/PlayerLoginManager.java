package FCP.Players;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;

import FCP.Config.ConfigManager;
import FCP.main.BasicInfo;

public class PlayerLoginManager implements Listener
{
	Plugin plugin;
	public PlayerLoginManager(Plugin p)
	{
		this.plugin = p;
	}
	/**When a player logs in check if they are a new player if not then check if their 
	current username is equal to the one they have if not then change it
	**/
	@EventHandler
	public void onLogin(PlayerLoginEvent event)
	{
		ConfigManager cm = new ConfigManager(plugin);
		if(cm.checkIfNewPlayer(event.getPlayer()))
		{
			cm.addPlayerToUuidRegistry(event.getPlayer());
			cm.createPlayerConfigFile(event.getPlayer());
			
			//Sets defaults
			cm.addStringToFile(ConfigManager.usersDir, event.getPlayer().getName(), BasicInfo.PLAYERFACTIONPATH, "null");
			cm.addIntToFile(ConfigManager.usersDir, event.getPlayer().getName(), BasicInfo.PLAYERLEVELPATH, 1);
			
		}
		
		else if(cm.getStringFromUuidEntry(BasicInfo.UUIDPATH + event.getPlayer().getUniqueId()) != event.getPlayer().getName())
		{
			cm.setStringToUuidFile(event.getPlayer(), BasicInfo.UUIDPATH + event.getPlayer().getUniqueId(), event.getPlayer().getName());
			cm.renamePlayerFile(event.getPlayer(), cm.getStringFromUuidEntry(BasicInfo.UUIDPATH + "." + event.getPlayer().getUniqueId()));
		}
	}
}
