package FCP.main;

import org.bukkit.ChatColor;

public class BasicInfo 
{

	//PATHS
	public static String FACTIONPATHREGULAR = "Faction.regular.";
	public static String FACTIONPATHLEADER = "Faction.leader";
	public static String FACTIONPATHID = "Faction.id";
	
	public static String UUIDPATH = "Players.";
	public static String PLAYERFACTIONPATH = "Info.faction";
	public static String PLAYERLEVELPATH = "Info.level";
	
	
	//COMMON MESSAGES
	public static String FACTIONTAG = ChatColor.BLUE + "[Faction] ";
}
