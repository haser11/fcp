package FCP.main;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import FCP.Commands.FactionChatCommand;
import FCP.Config.ConfigManager;
import FCP.Factions.Faction;
import FCP.Players.CommandManager;
import FCP.Players.MedicListener;
import FCP.Players.PlayerLoginManager;
import FCP.World.WorldListeners;

public class FCP extends JavaPlugin
{
	/**
	 * FCP is the official plugin for the FallenCraft Roleplay server. It is currently being developed and maintained by haser11.
	 * FCP is a private plugin only for use in the FallenCraft RPG server.
	 */
	
	@Override
	public void onEnable()
	{
		Bukkit.getLogger().log(Level.INFO, "FCP has been enabled!");
		ConfigManager.checkIfNewServer();
		ConfigManager.createUuidRegistry();
		
		//Class Registry
		new ConfigManager(this);
		new Faction(this);
		new MedicListener(this);
		new FactionChatCommand(this);
		
		
		//Listener Registry
		PluginManager manager = Bukkit.getServer().getPluginManager();
		manager.registerEvents(new PlayerLoginManager(this), this);
		manager.registerEvents(new WorldListeners(this), this);
		manager.registerEvents(new MedicListener(this), this);
		manager.registerEvents(new FactionChatCommand(this), this);
		
		new Faction("Guardians", 0);
		new Faction("Farriers", 1);
		new Faction("Physicians", 2);
		new Faction("Agriculturalists", 3);
		
		//Timer Registry
		MedicListener.startTimer();
		
		//Command Registry
		getCommand("fcp").setExecutor(new CommandManager(this));
		getCommand("faction").setExecutor(new FactionChatCommand(this));

	}
	
	@Override
	public void onDisable()
	{
		Bukkit.getLogger().log(Level.INFO, "FCP has been disabled");
		Bukkit.getScheduler().cancelTasks(this);
	}
	
}
