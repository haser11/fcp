package FCP.World;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.block.Sign;
import org.bukkit.plugin.Plugin;

import FCP.Factions.Faction;

public class WorldListeners implements Listener
{
	Plugin plugin;
	public WorldListeners(Plugin p)
	{
		this.plugin = p;
	}
   
	@EventHandler
	public void onPlaceSign(SignChangeEvent event)
	{
		if(event.getLine(0).equalsIgnoreCase("[Faction]"))
		{
			event.getLine(0).replace("Faction", ChatColor.AQUA + "Faction");
		}
	}
	
	@EventHandler
	public void onClickSign(PlayerInteractEvent event)
	{
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(event.getClickedBlock().equals(Material.SIGN) || event.getClickedBlock().equals(Material.WALL_SIGN) || event.getClickedBlock().equals(Material.SIGN_POST))
			{
				Sign sign = (Sign) event.getClickedBlock();
				if(sign.getLine(0).equalsIgnoreCase("[" + ChatColor.AQUA + "Faction" + ChatColor.RESET + "]"))
				{
					new Faction(sign.getLine(1), event.getPlayer(), sign.getLine(2));
				}
			}
		}
	}
	
}
