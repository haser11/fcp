package FCP.Factions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import FCP.Config.ConfigManager;
import FCP.main.BasicInfo;

public class Faction 
{
	Plugin plugin;
	
	public Faction(Plugin p)
	{
		this.plugin = p;
	}

	public Faction(String name, int id)
	{
		ConfigManager cm = new ConfigManager(plugin);
		cm.createFile(ConfigManager.dir, name);
		cm.addIntToFile(ConfigManager.dir, name, BasicInfo.FACTIONPATHID, id);
	}
	public Faction(String factionName, Player p, String rank)
	{
		ConfigManager cm = new ConfigManager(plugin);
		if(rank.equalsIgnoreCase("regular"))
		{
		cm.addStringToFile(ConfigManager.dir, factionName, BasicInfo.FACTIONPATHREGULAR + p.getName(), "ThisIsJustAFixForABugSoYeahHopefullyIFixThis");
		cm.saveConfig();
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/pex user " + p.getName() + " prefix " + "&f[&1" + factionName + "6f]");
		}
		else if(rank.equalsIgnoreCase("leader"))
		{
		cm.addStringToFile(ConfigManager.dir, factionName, BasicInfo.FACTIONPATHLEADER, p.getName());
		cm.saveConfig();
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/pex user " + p.getName() + " prefix " + "&f[&1" + factionName + "6f]");
		}
	}
}
