package FCP.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

import FCP.Config.ConfigManager;
import FCP.main.BasicInfo;

public class FactionChatCommand implements CommandExecutor, Listener
{

	Plugin plugin;
	List<String> chatList = new ArrayList<String>();
	
	public FactionChatCommand(Plugin p)
	{
		this.plugin = p;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) 
	{
		if(command.getName().equalsIgnoreCase("faction"))
		{
			if(args.length == 2)
			{
				if(args[0].equalsIgnoreCase("chat"))
				{
					if(args[1].equalsIgnoreCase("on"))
					{
						chatList.add(sender.getName());
						sender.sendMessage(BasicInfo.FACTIONTAG + ChatColor.GREEN + "You have successfully entered faction chat!");
						return true;
					}
					
					else if(args[1].equalsIgnoreCase("off"))
					{
						if(chatList.contains(sender.getName()))
						{
							chatList.remove(sender.getName());
							sender.sendMessage(BasicInfo.FACTIONTAG + ChatColor.GREEN + "You have successfully left faction chat!");
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event)
	{
		if(chatList.contains(event.getPlayer().getName()))
		{
			ConfigManager cm = new ConfigManager(plugin);
			
			String factionName = cm.getString(event.getPlayer(), BasicInfo.PLAYERFACTIONPATH);
			Player[] playerList = Bukkit.getOnlinePlayers();
			
			for(int i = 0; i < Bukkit.getOnlinePlayers().length; i++)
			{
				if(cm.getString(playerList[i], BasicInfo.PLAYERFACTIONPATH).equalsIgnoreCase(factionName))
				{
					String oldMessage = event.getMessage();
					oldMessage = ChatColor.GREEN + "[FACTIONCHAT] " + ChatColor.YELLOW + oldMessage;
					
					event.setMessage(oldMessage);
				}
				else
				{
					event.setCancelled(true);
				}
			}
		}
	}

}
