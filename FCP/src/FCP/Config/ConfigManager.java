package FCP.Config;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;


public class ConfigManager
{
	Plugin plugin;
	public static File dir = new File("plugins" + File.separator + "FCP");
	public static File usersDir = new File(dir + File.separator + "users" + File.separator);
	
	static File uuidEntry = new File(dir + File.separator + "uuids.yml");
	
	static FileConfiguration config = null;
	static FileConfiguration config2 = null;
	
	public ConfigManager(Plugin p)
	{
		this.plugin = p;
	}
	
	//Creates a new file in the provided directory
	public void createFile(File directory, String name)
	{
		File newFile = new File(directory + File.separator + name + ".yml");
		if(!newFile.exists())
		{
			try
			{
				newFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public File getFile(File directory, String name)
	{
		File newFile = new File(directory + File.separator + name + ".yml");
		return newFile;
	}
	
	//Adds an entry to the provided file
	public void addStringToFile(File directory, String name, String path, String value)
	{
		File newFile = new File(directory + File.separator + name + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(newFile);
		
		ConfigManager.config.set(path, value);
		
		try
		{
			ConfigManager.config.save(newFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//Adds an entry to the provided file
	public void addIntToFile(File directory, String name, String path, int value)
	{
		File newFile = new File(directory, File.separator + name + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(newFile);
		
		ConfigManager.config.set(path, value);
		
		try
		{
			ConfigManager.config.save(newFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//Adds an entry to the provided file
	public void addBoolToFile(File directory, String name, String path, boolean value)
	{
		File newFile = new File(directory, File.separator + name + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(newFile);
		
		ConfigManager.config.set(path, value);
		
		try
		{
			ConfigManager.config.save(newFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//Checks if the server has the default config paths if not then it creates one
	public static void checkIfNewServer()
	{
		if(!usersDir.exists())
		{
			try
			{
				Bukkit.getServer().getLogger().log(Level.WARNING, "No config for FCP found! Atempting to create one...");
				usersDir.mkdirs();
				Bukkit.getServer().getLogger().log(Level.INFO, "Successfully created config for FCP!");
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Bukkit.getLogger().log(Level.WARNING, "Uh oh, haser broke something");
			}
		}
	}
	
	//Checks if the server has the UUID registry file if not then it creates one
	public static void createUuidRegistry()
	{
		if(!uuidEntry.exists())
		{
			try 
			{
				uuidEntry.createNewFile();
				config = YamlConfiguration.loadConfiguration(uuidEntry);
				config.get("Players");
				config.save(uuidEntry);
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	//Adds a player to the uuid registry
	public void addPlayerToUuidRegistry(Player p)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		ConfigManager.config.set("Players." + p.getUniqueId(), p.getName());
		try 
		{
			ConfigManager.config.save(uuidEntry);
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	//Checks if a player is registered in the UUID registry
	public boolean checkIfNewPlayer(Player p)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		if(ConfigManager.config.getString("Players." + p.getUniqueId(), null) == null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//Creates a players own config file
	public void createPlayerConfigFile(Player p)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
			File pFile = new File(dir + File.separator + "users" + File.separator + ConfigManager.config.getString("Players." + p.getUniqueId()).toString() + ".yml");
			try
			{
			pFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	
	
	/**
	 * 
	 * 
	 * All the methods for adding info to files
	 * 
	 */
	
	public void setString(Player p, String path, String value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		ConfigManager.config.set(path, value);
	
		try
		{
			ConfigManager.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setInt(Player p, String path, int value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		ConfigManager.config.set(path, value);
	
		try
		{
			ConfigManager.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setBoolean(Player p, String path, boolean value)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		ConfigManager.config.set(path, value);
	
		try
		{
			ConfigManager.config.save(pFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void setStringToUuidFile(Player p, String path, String value)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		ConfigManager.config.set(path, value);
	}

	
	/**
	 * 
	 * 
	 * All the methods for retrieving player info
	 * 
	 */
	
	public String getString(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (String) ConfigManager.config.get(path);
	}
	
	public boolean getBoolean(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (boolean) ConfigManager.config.get(path);
	}
	
	public int getInt(Player p, String path)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
		
		return (int) ConfigManager.config.get(path);
	}
	
	public String getStringFromUuidEntry(String path)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		return (String) ConfigManager.config.get(path);
	}
	public String getStringFromFile(File file, String name, String path)
	{
		ConfigManager.config = YamlConfiguration.loadConfiguration(new File(file + name + ".yml"));
		return (String) ConfigManager.config.get(path);
	}
	
	
	public void renamePlayerFile(Player p, String newName)
	{
		File pFile = new File(usersDir + File.separator + p.getName() + ".yml");
		File newFile = new File(usersDir + File.separator + newName + ".yml");
		
		pFile.renameTo(newFile);
	}
	
	//Saves the config files for the server
	public void saveConfig()
	{
		for(int i = 0; i < Bukkit.getServer().getOnlinePlayers().length; i++)
		{
			Player[] players = Bukkit.getServer().getOnlinePlayers();
			File pFile = new File(usersDir + File.separator + players[i].getName() + ".yml");
			ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
			
			try
			{
				ConfigManager.config.save(pFile);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		try
		{
			ConfigManager.config.save(uuidEntry);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	//Loads the config files for the server
	public void loadConfig()
	{
		for(int i = 0; i < Bukkit.getServer().getOnlinePlayers().length; i++)
		{
			Player[] players = Bukkit.getServer().getOnlinePlayers();
			File pFile = new File(usersDir + File.separator + players[i].getName() + ".yml");
			ConfigManager.config = YamlConfiguration.loadConfiguration(pFile);
			
			try
			{
				ConfigManager.config.load(pFile);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		ConfigManager.config = YamlConfiguration.loadConfiguration(uuidEntry);
		
		try
		{
			ConfigManager.config.load(uuidEntry);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public File getFile(String player)
	{
		File file = new File(usersDir + File.separator + player + ".yml");
		return file;
	}
}